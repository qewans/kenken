/**
 * Created by quentin on 24/09/2017.
 */
angular.module('kenkenApp', [
    // ...which depends on the `kenken` module
    'kenken'
]);
