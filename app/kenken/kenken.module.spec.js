/**
 * Created by quentin on 23/09/2017.
 */
'use strict';


describe('KenkenController', function() {

    beforeEach(module('kenken'));

    it('should create a `phones` model with 3 phones', inject(function($controller) {
        var scope = {};
        var ctrl = $controller('KenkenController', {$scope: scope});

        expect(scope.grid.length).toBe(4);
    }));

});