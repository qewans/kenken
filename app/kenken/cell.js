/**
 * Created by quentin on 22/09/2017.
 */
class Cell {

    constructor(solution, label, borders) {
        this.solution = solution;
        this.label = label;
        this.borders = borders;
        this.candidates = [];
        this.impossible = [];
    }

    toogleAnswer(value, isPossible) {
        var index = this.candidates.indexOf(value);
        if (index == -1) {
            this.candidates.push(value);
        } else {
            this.candidates.splice(index, 1);
        }
        this.candidates.sort();
    }

    hasCandidates() {
        return this.candidates.length > 1;
    }

    hasAnswer() {
        return this.candidates.length == 1;
    }
    reset() {
        this.candidates = [];
    }

    addAllCandidates(size, impossible) {
        for (var i = 1; i <= size; i++) {
            this.candidates[i - 1] = i;
        }
        this.impossible = impossible;
    }

    removeAllCandidates(size) {
        this.candidates[i - 1] = i;
    }

    getAnswer() {
        if (!this.candidates[0]) return "";
        return this.candidates[0];
    }

    isImpossible(candidate){
        return this.impossible.indexOf(candidate) != -1;
    }
}
