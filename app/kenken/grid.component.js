/**
 * Created by quentin on 24/09/2017.
 */
'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
module('kenken').
component('kgrid', {
    templateUrl: 'kenken/grid.template.html',
    controller: ['grid', function KenkenController(grid) {
        this.size = 4;
        this.grid = grid;

        this.select = function (x, y) {
            grid.selected = [x, y];
        }

        this.setValue = function (value) {
            this.grid.setAnswer(value);
        }
        this.newGame = function (value) {
            this.grid.resetAnswer();
        }
        this.reset = function () {
            this.grid.reset();
        }

        this.hasBorder = function (side, code) {
            return (code) ? code.charAt("trbl".indexOf(side)) == "1" : false;
        };
        this.getBorderSelectors = function (code) {
            if (!code) return "";
            var borders = [];
            if (code.charAt(0) == "1") borders.push("k-bt");
            if (code.charAt(1) == "1") borders.push("k-br");
            if (code.charAt(2) == "1") borders.push("k-bb");
            if (code.charAt(3) == "1") borders.push("k-bl");
            return borders.join(" ");
        }
        this.getHighlight = function (x,y){
            if(grid.selected[0] == x && grid.selected[1] == y) return "k-selected";
            return this.grid.isHightlight(x,y) ? "k-highlight" : "";
        }

        this.getImpossibleAnswerClass = function(cell, answer){
            if(cell.hasAnswer() && cell.isImpossible(answer)){
                return "k-wrong";
            }
            return "";
        }

        this.getCandidateClass = function(cell, candidate){
            if(cell.isImpossible(candidate)){
                return "k-impossible";
            }
            return "k-candidate";
        }

    }]
});
