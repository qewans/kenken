/**
 * Created by quentin on 22/09/2017.
 */
class Grid {
    constructor() {
        this.grid = [
            [
                {"l": "9+", "v": 3, "b": "1001"},
                {"l": "", "v": 5, "b": "1110"},
                {"l": "1-", "v": 7, "b": "1011"},
                {"l": "", "v": 8, "b": "1110"},
                {"l": "17+", "v": 4, "b": "1101"},
                {"l": "60*", "v": 1, "b": "1001"},
                {"l": "", "v": 6, "b": "1110"},
                {"l": "9+", "v": 2, "b": "1101"}
            ],
            [
                {"l": "", "v": 1, "b": "0111"},
                {"l": "40*", "v": 8, "b": "1011"},
                {"l": "", "v": 5, "b": "1110"},
                {"l": "", "v": 6, "b": "1011"},
                {"l": "", "v": 7, "b": "0110"},
                {"l": "", "v": 2, "b": "0101"},
                {"l": "", "v": 3, "b": "1011"},
                {"l": "", "v": 4, "b": "0110"}
            ],
            [
                {"l": "1-", "v": 2, "b": "1011"},
                {"l": "", "v": 3, "b": "1110"},
                {"l": "192*", "v": 6, "b": "1011"},
                {"l": "", "v": 4, "b": "1010"},
                {"l": "", "v": 8, "b": "1110"},
                {"l": "", "v": 5, "b": "0111"},
                {"l": "7/", "v": 7, "b": "1011"},
                {"l": "", "v": 1, "b": "1110"}
            ],
            [
                {"l": "3-", "v": 4, "b": "1011"},
                {"l": "", "v": 7, "b": "1110"},
                {"l": "2", "v": 2, "b": "1111"},
                {"l": "10*", "v": 1, "b": "1011"},
                {"l": "", "v": 5, "b": "1100"},
                {"l": "21+", "v": 6, "b": "1001"},
                {"l": "", "v": 8, "b": "1110"},
                {"l": "13+", "v": 3, "b": "1101"}
            ],
            [
                {"l": "2-", "v": 5, "b": "1101"},
                {"l": "1", "v": 1, "b": "1111"},
                {"l": "5-", "v": 8, "b": "1011"},
                {"l": "", "v": 3, "b": "1110"},
                {"l": "", "v": 2, "b": "0111"},
                {"l": "", "v": 7, "b": "0111"},
                {"l": "", "v": 4, "b": "1011"},
                {"l": "", "v": 6, "b": "0110"}
            ],
            [
                {"l": "", "v": 7, "b": "0111"},
                {"l": "13+", "v": 6, "b": "1001"},
                {"l": "", "v": 1, "b": "1100"},
                {"l": "17+", "v": 2, "b": "1101"},
                {"l": "288*", "v": 3, "b": "1011"},
                {"l": "", "v": 4, "b": "1100"},
                {"l": "5", "v": 5, "b": "1111"},
                {"l": "3-", "v": 8, "b": "1101"}
            ],
            [
                {"l": "192*", "v": 8, "b": "1101"},
                {"l": "", "v": 2, "b": "0011"},
                {"l": "", "v": 4, "b": "0110"},
                {"l": "", "v": 7, "b": "0101"},
                {"l": "6*", "v": 6, "b": "1101"},
                {"l": "", "v": 3, "b": "0101"},
                {"l": "2/", "v": 1, "b": "1101"},
                {"l": "", "v": 5, "b": "0111"}
            ],
            [
                {"l": "", "v": 6, "b": "0011"},
                {"l": "", "v": 4, "b": "1110"},
                {"l": "", "v": 3, "b": "1011"},
                {"l": "", "v": 5, "b": "0110"},
                {"l": "", "v": 1, "b": "0111"},
                {"l": "", "v": 8, "b": "0111"},
                {"l": "", "v": 2, "b": "0111"},
                {"l": "7", "v": 7, "b": "1111"}
            ],
        ];
        this.selected = [0, 0];
        this.cells = Array();
        this.size = this.grid.length;
        this.lastIndex = this.grid.length - 1;
        for (var row of this.grid) {
            var cellRow = Array();
            for (var cell of row) {
                cellRow.push(new Cell(cell.v, cell.l, cell.b));
            }
            this.cells.push(cellRow);
        }
    }

    moveLeft(controlDown) {
        var newIndex = (controlDown) ? 0 : this.selected[1] - 1;
        this.selected = [this.selected[0], this.selected[1] > 0 ? newIndex : 0];
    }

    moveUp(controlDown) {
        this.selected = [this.selected[0] > 0 ? this.selected[0] - 1 : 0, this.selected[1]];
    }

    moveRight(controlDown) {
        this.selected = [this.selected[0], this.selected[1] < this.lastIndex ? this.selected[1] + 1 : this.lastIndex];
    }

    moveDown(controlDown) {
        this.selected = [this.selected[0] < this.lastIndex ? this.selected[0] + 1 : this.lastIndex, this.selected[1]];
    }


    getSelectedCell() {
        return this.grid[this.selected[0]][this.selected[1]];
    }
    updateCandidates(x, y){
        for (var i = 0; i < this.size; i++) {
            if (i != y) {
                this.cells[x][i].impossible = this.getImpossibleAnswer(x, i);
            }
            if (i != x) {
                this.cells[i][y].impossible = this.getImpossibleAnswer(i, y);
            }
        }
    }

    setAnswer(value) {
        this.cells[this.selected[0]][this.selected[1]].toogleAnswer(value);
        this.updateCandidates(this.selected[0], this.selected[1]);
    }


    reset() {
        for (var row of this.cells) {
            for (var cell of row) {
                cell.reset();
            }
        }
    }


    removeAllCandidates() {
        this.cells[this.selected[0]][this.selected[1]].reset();
        this.updateCandidates(this.selected[0], this.selected[1]);
    }

    getImpossibleAnswer(x, y) {
        var impossible = [];
        for (var i = 0; i < this.size; i++) {
            if (i != y) {
                var cell = this.cells[x][i];
                if (cell.hasAnswer()) {
                    if (impossible.indexOf(cell.getAnswer()) == -1) {
                        impossible.push(cell.getAnswer());
                    }
                }
            }
            if (i != x) {
                var cell = this.cells[i][y];
                if (cell.hasAnswer()) {
                    if (impossible.indexOf(cell.getAnswer()) == -1) {
                        impossible.push(cell.getAnswer());
                    }
                }
            }
        }
        return impossible.sort();
    }

    addAllCandidates() {
        var x = this.selected[0];
        var y = this.selected[1];
        var impossible = this.getImpossibleAnswer(x, y);
        this.cells[x][y].addAllCandidates(this.grid.length, impossible);
    }

    isHightlight(xX,yY){
        var x = this.selected[0];
        var y = this.selected[1];
        if(!(x==xX && y==yY)){
            if(x == xX || y == yY ){
                return true;
            }
        }
        return false;
    }
}

































