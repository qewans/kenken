/**
 * Created by quentin on 21/09/2017.
 */
var app = angular.module('kenken', []);
app.factory('grid', function () {
    return new Grid();
});

app.controller('KeyController', function (grid) {
    this.grid = grid;
    this.keyUp = function (event) {
        var code = event.keyCode;
        if (code === 37) this.grid.moveLeft();
        else if (code === 38) this.grid.moveUp();
        else if (code === 39) this.grid.moveRight();
        else if (code === 40) this.grid.moveDown();
        // Input Numbers
        else if (code >= 97 && code <= 97 + this.grid.lastIndex) {
            this.grid.setAnswer(code - 96);
        }
        // Candidates
        else if (code === 107) this.grid.addAllCandidates();
        else if (code === 109) this.grid.removeAllCandidates();
        // Reset
        else if (code ===46) this.grid.reset();
    }

});

